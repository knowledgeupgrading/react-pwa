importScripts("https://storage.googleapis.com/workbox-cdn/releases/5.1.2/workbox-sw.js");

// Turn on logging
workbox.setConfig({
    debug: true
});

// Updating SW lifecycle to update the app after user triggered refresh
workbox.core.skipWaiting();
workbox.core.clientsClaim();

// PRECACHING

// We inject manifest here using "workbox-build" in workbox-build.js
workbox.precaching.precacheAndRoute(self.__WB_MANIFEST);


// RUNTIME CACHING

// API with network-first strategy
workbox.routing.registerRoute(
    "https://newsapi.org/v2/top-headlines?country=ua",
    new workbox.strategies.CacheFirst()
);
