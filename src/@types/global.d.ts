declare global {
    interface BaseAction<T extends string> {
        type: T;
        payload?: any;
        meta?: any;
        error?: boolean;
    }

    interface Reducer<T, A extends string = string, P> {
        (state: T | undefined, action: BaseAction<A> & P): T;
    }

    type ReducerActions = 'ACTION' | 'ERROR' | 'SUCCESS' | 'CLEAR';

}
