declare module 'redux-define' {
    interface DefineAction {
        defineAction<T extends string>(
            base: string,
            subActions: T[],
            namespace: string,
        ): { [U in T]: U } & { ACTION: 'ACTION' };
    }
    const ModuleExport: DefineAction;

    export = ModuleExport;
}
