import React, {lazy, Suspense} from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';

import {Loading, Layout} from './components';

const lazyImport = (filename: string) => {
    return lazy(() => import(`${filename}`));
};

const routes = (
    <Switch>
        <Route
            path="/article/:id"
            component={lazyImport("./pages/Articles/SingleArticle")}
        />
        <Route
            exact
            path="/articles"
            component={lazyImport("./pages/Articles/Articles")}
        />
        <Route
            exact
            path="/"
            component={lazyImport("./pages/Home")}
        />
        <Redirect to="/" />
    </Switch>
);

export const App: React.FC = () => {
    return (
        <Layout>
            <Suspense fallback={<Loading />}>
                {routes}
            </Suspense>
        </Layout>
    );
};