import * as api from "./newsapi";
import {newsApiInstance} from './instances';
import * as apiTypes from './types';

export {api, newsApiInstance, apiTypes};