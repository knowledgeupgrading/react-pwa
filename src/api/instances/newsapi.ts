import axios from "axios";

import {apiKey} from '../../config';

const instance = axios.create({
    baseURL: "https://newsapi.org/v2/",
    headers: {
        "X-Api-Key": apiKey
    }
});

export default instance;