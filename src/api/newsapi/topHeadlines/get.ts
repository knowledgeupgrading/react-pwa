import {AxiosResponse} from 'axios';
import {stringify} from 'qs';

import {newsApiInstance as http} from '../../instances';
import {ITopHeadlinesDTO} from '../../types';

interface IQueryParams {
    apiKey: string;
    country?: string;
    category?: string;
    sources?: string;
    q?: string;
    pageSize?: number;
    page?: number;
}

export const getTopHeadlines = (queryParams: IQueryParams): Promise<AxiosResponse<ITopHeadlinesDTO>> => (
    http.get(`top-headlines?${stringify(queryParams, {
        arrayFormat: 'repeat',
        skipNulls: true,
    })}`)
);