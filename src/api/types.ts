enum EStatus {
    Ok = 'ok',
    Error = 'error'
}

export interface ITopHeadlinesDTO {
    status: EStatus;
    totalResults: number;
    articles: ITopHeadlinesArticles[];
}

export interface ITopHeadlinesArticles {
    innerId: string;
    source: {
        id: string;
        name: string;
    };
    code?: number;
    message?: string;
    author: string;
    title: string;
    description: string;
    url: string;
    urlToImage: string;
    publishedAt: string;
    content: string;
}