import React from 'react';

import './Layout.css';
import "../../assets/css/bootstrap.css";
import {Header} from './components';

export const Layout: React.FC = ({children}) => {
    return (
        <div className="layout">
            <Header />
            {children}
        </div>
    );
};