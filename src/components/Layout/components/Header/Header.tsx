import React from 'react';
import {Link} from 'react-router-dom';

import './Header.css';
import logo from "../../../../assets/logo.png";

export const Header: React.FC = () => {
    return (
        <div className="header">
            <div className="logo">
                <img src={logo} />
            </div>
            <div className="menu">
                <div>
                    <Link to="/">Home</Link>
                </div>
                <div>
                    <Link to="/articles">Articles</Link>
                </div>
            </div>
        </div>
    );
};
