import React, {FC} from "react";

import './Loading.css';

export const Loading: FC = () => {
    return (
        <div className="loader">
            <div className="bar"></div>
        </div>
    );
};