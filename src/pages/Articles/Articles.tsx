import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {isEmpty} from 'lodash';

import ArticleItem from './components/ArticleItem';
import {topHeadlinesActions as actions} from '../../store/actions';
import {topHeadlinesSelectors as selectors} from '../../store/selectors';
import {Loading} from "../../components";

const Articles: React.FC = () => {

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(actions.fetchTopHeadlines({
            country: "ua",
        }));
    }, []);

    const articles = useSelector(selectors.getTopHeadlines());
    const isLoading = useSelector(selectors.isLoading());

    return (
        <>
            {isLoading && <Loading />}
            {(!isLoading && !isEmpty(articles)) && (
                <div className="row">
                    {articles.map((article: any, key: number) => (
                        <div key={key} className="col-lg-4 col-md-6 col-sm-6">
                            <ArticleItem {...article} />
                        </div>
                    ))}
                </div>
            )}
        </>
    );
};
export default Articles;
