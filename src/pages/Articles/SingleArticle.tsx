import React, {useEffect} from 'react';
import {useSelector} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {isNil} from 'lodash';
import moment from 'moment';

import './components/Article.css';
import {topHeadlinesSelectors as selectors} from "../../store/selectors";

interface IProps {
    match: any;
    history: any;
}

// @ts-ignore
const SingleArticle: React.FC<IProps> = withRouter(({match, history}) => {

    const article = useSelector(selectors.getTopHeadlinesSingleArticle(match.params.id));

    useEffect(() => {
        isNil(article) && history.push('/articles');
    });

    console.log(article);
    return article && (
        <div className="post">
            <div className="post-img">
                <img
                    src={article.urlToImage}
                    alt=""
                />
            </div>
            <div className="post-body">
                <div className="post-meta">
                    <div className="post-category cat-1">{article.author ? article.author : 'Unknown'}</div>
                    <span className="post-date">{moment(article.publishedAt).format('LL')}</span>
                </div>
                <h3 className="post-title">{article.title}</h3>
                <div>
                    {article.description}
                </div>
            </div>
        </div>
    );
});

export default SingleArticle;
