import React from 'react';
import {Link} from 'react-router-dom';
import moment from 'moment';

import './Article.css';
import {apiTypes} from '../../../api';

interface IProps extends apiTypes.ITopHeadlinesArticles {
}

const ArticleItem: React.FC<IProps> = ({author, urlToImage, title, publishedAt, innerId}) => {
    return (
        <div className="post">
            <Link className="post-img" to={`/article/${innerId}`}>
                <img
                    src={urlToImage}
                    alt=""
                />
            </Link>
            <div className="post-body">
                <div className="post-meta">
                    <div className="post-category cat-1">{author ? author : 'Unknown'}</div>
                    <span className="post-date">{moment(publishedAt).format('LL')}</span>
                </div>
                <h3 className="post-title"><Link to={`/article/${innerId}`}>{title}</Link></h3>
            </div>
        </div>
    );
};
export default ArticleItem;
