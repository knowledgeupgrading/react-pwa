import React from 'react';

import pwa from '../assets/pwa.jpg';

const Home: React.FC = () => {
    return (
        <div>
            <img style={{width: "100%"}} src={pwa} />
            <p>
                Progressive Web Apps (PWA) are built and enhanced with modern APIs to deliver native-like capabilities,
                reliability, and installability while reaching anyone, anywhere, on any device with a single codebase.
                To
                help you create the best possible experience, use the core and optimal checklists and recommendations to
                guide you.
                guide you.
            </p>
        </div>
    );
};

export default Home;