import {combineReducers} from "redux";

import {reducer as topHeadlines} from './topHeadlines';

const rootReducer = combineReducers({
    topHeadlines,
});

export default rootReducer;
