import {all} from "redux-saga/effects";

import {saga as topHeadlines} from './topHeadlines';

export default function* rootSaga() {
    yield all([
        topHeadlines(),
    ]);
}