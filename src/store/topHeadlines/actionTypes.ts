export enum FETCH {
    ACTION = 'top-headlines/FETCH',
    SUCCESS = 'top-headlines/FETCH_SUCCESS',
    ERROR = 'top-headlines/FETCH_ERROR',
}