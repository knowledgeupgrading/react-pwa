import {FETCH} from './actionTypes';
import {apiTypes} from '../../api';

export const fetchTopHeadlines = (queryParams: any) => {
    return {
        type: FETCH.ACTION,
        queryParams
    };
};

export const fetchTopHeadlinesSuccess = (response: apiTypes.ITopHeadlinesDTO) => {
    return {
        type: FETCH.SUCCESS,
        topHeadlines: response
    };
};

export const fetchTopHeadlinesError = (error: any) => {
    return {
        type: FETCH.ERROR,
        error
    };
};