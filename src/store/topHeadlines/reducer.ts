import {FETCH} from "./actionTypes";

const initialState: object = {
    articles: null,
    loading: false,
    error: null
};

const fetchTopHeadlines = (state: object) => {
    return {
        ...state,
        loading: true
    }
};

const fetchTopHeadlinesSuccess = (state: object, action: any) => {
    return {
        ...state,
        articles: action.topHeadlines.articles,
        loading: false
    };
};

const fetchTopHeadlinesError = (state: object) => {
    return {...state, loading: false};
};

export const reducer = (state = initialState, action: any) => {
    switch (action.type) {
        case FETCH.ACTION:
            return fetchTopHeadlines(state);
        case FETCH.SUCCESS:
            return fetchTopHeadlinesSuccess(state, action);
        case FETCH.ERROR:
            return fetchTopHeadlinesError(state);
        default:
            return state;
    }
};
