import {put, call, takeLatest} from "redux-saga/effects";
import shortid from 'shortid';

import {api} from '../../api';
import {FETCH} from "./actionTypes";
import {fetchTopHeadlinesSuccess, fetchTopHeadlinesError} from './actions';

function* fetchTopHeadlinesWorker({queryParams}: any) {
    try {
        const response = yield call(api.getTopHeadlines, queryParams);
        const articles = response.data.articles.map((item: any) => {
            return {
                innerId: shortid.generate(),
                ...item
            };
        });
        yield put(fetchTopHeadlinesSuccess({
            ...response.data,
            articles
        }));
    } catch (error) {
        yield put(fetchTopHeadlinesError(error));
    }
}

export function* saga() {
    yield takeLatest(FETCH.ACTION, fetchTopHeadlinesWorker);
}
