import {DefaultRootState} from 'react-redux';
import {get, isNull} from 'lodash';

export const isLoading = () => (state: DefaultRootState) => {
    return get(state, `topHeadlines.loading`);
};

export const getTopHeadlines = () => (state: DefaultRootState) => {
    return get(state, `topHeadlines.articles`);
};

export const getTopHeadlinesSingleArticle = (id: string) => (state: DefaultRootState) => {
    const articles = get(state, `topHeadlines.articles`);
    return !isNull(articles) ? articles.filter((item: any) => item.innerId === id)[0] : null;
};