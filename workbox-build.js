const { injectManifest } = require("workbox-build");

const workboxConfig = {
    globDirectory: "dist",
    globPatterns: [
        "index.html",
        "static/**/*",
    ],
    swSrc: "serviceWorker.js",
    swDest: "dist/service-worker.js"
};

injectManifest(workboxConfig)
    .then(({ count, size }) => {
        console.log(`Generated ${workboxConfig.swDest}, which will precache ${count} files, totaling ${size} bytes.`)
    }).catch(error => console.log(`>>> ${error}`));